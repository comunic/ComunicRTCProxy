package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/url"

	"gopkg.in/yaml.v2"
)

// Config stores application configuration
type Config struct {
	Secure   bool
	Hostname string
	Port     int
	Path     string
	Token    string

	// Optional information to inject proxy reachability (can be left empty)
	AccessIP string

	// Optionaly restrict Ports range (can be left to 0)
	PortStart int
	PortEnd   int

	// Amount of time to wait before automatically
	//quitting the application when the websocket
	// connection was interrupted
	Restart int
}

// Get the URL associated with the configuration
func (c *Config) getURL() url.URL {

	// Adapt scheme
	scheme := "ws"
	if c.Secure {
		scheme = "wss"
	}

	return url.URL{
		Scheme:   scheme,
		Host:     fmt.Sprintf("%s:%d", c.Hostname, c.Port),
		Path:     c.Path,
		RawQuery: "token=" + c.Token,
	}
}

// Load the configuration
func loadConfig(path string) Config {

	// Load data
	dat, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatalf("Could not read configuration: %v", err)
	}

	// Decode data
	conf := Config{}
	err = yaml.UnmarshalStrict(dat, &conf)
	if err != nil {
		log.Fatalf("Could not decode configuration: %v", err)
	}

	return conf
}
