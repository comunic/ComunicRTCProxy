/// Call configuration
///
/// This files handles dynamic call configuration
///
/// @author Pierre HUBERT

package main

import (
	"github.com/pion/webrtc/v2"
)

type callConfig struct {
	iceServers []webrtc.ICEServer
	allowVideo bool
}

// This configuration is intialized as soon as
// websocket connection is made
var callConf = callConfig{}

/// Load configuration
func setCallConfig(data map[string]interface{}) {

	// Process ice servers
	servers := data["iceServers"].([]interface{})
	for _, server := range servers {
		callConf.iceServers = append(callConf.iceServers, webrtc.ICEServer{
			URLs: []string{server.(string)},
		})
	}

	// Update allow video status
	callConf.allowVideo = data["allowVideo"].(bool)
}
