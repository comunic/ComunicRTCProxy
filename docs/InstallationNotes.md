# Installation

## Create user
```bash
sudo adduser --disabled-login comunic_rtc_proxy
```

## Install go & build project
As `comunic_rtc_proxy`, download & install go:
```bash
# Login as comunic_rtc_proxy
sudo -u comunic_rtc_proxy bash
cd

# Download & extract go
wget https://dl.google.com/go/go1.14.2.linux-armv6l.tar.gz
tar -xf go1.14.2.linux-armv6l.tar.gz

# Add Go to path
export PATH=$PATH:/home/comunic_rtc_proxy/go/bin
echo "export PATH=\$PATH:/home/comunic_rtc_proxy/go/bin" >> .bashrc

# Generate go workspace
mkdir -p go_workspace/src
export GOPATH=$(pwd)/go_workspace
echo "export GOPATH=\$HOME/go_workspace" >> .bashrc

# Get sources & compile them
cd $GOPATH
git clone https://gitlab.com/comunic/ComunicRTCProxy comunic_rtc_proxy
cd comunic_rtc_proxy/

go get
go build
```

## Generate custom configuration
Copy the default configuration file `config.yaml` into `config.custom.yaml` and customize it to fit your needs.

## Install the proxy as a service
Create the file `/etc/systemd/system/comunic_rtc_proxy.service`:
```conf
[Unit]
Description=Comunic RTC proxy
After=syslog.target
After=network.target

[Service]
RestartSec=2s
Type=simple
User=comunic_rtc_proxy
Group=comunic_rtc_proxy
WorkingDirectory=/home/comunic_rtc_proxy/
ExecStart=/home/comunic_rtc_proxy/go_workspace/comunic_rtc_proxy/comunic_rtc_proxy /home/comunic_rtc_proxy/go_workspace/comunic_rtc_proxy/config.custom.yaml
Restart=always
Environment=USER=comunic_rtc_proxy

[Install]
WantedBy=multi-user.target
```

Enable & start the new service:
```bash
sudo systemctl enable comunic_rtc_proxy.service
sudo systemctl start comunic_rtc_proxy.service
```
