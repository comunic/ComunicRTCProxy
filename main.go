package main

import (
	"fmt"
	"log"
	"os"
	"time"
)

func main() {

	println("Comunic RTC Proxy. (c) Pierre Hubert 2020")

	// Command line args
	if len(os.Args) != 2 {
		fmt.Printf("Usage: %s <config>", os.Args[0])
	}

	// First, load the config
	conf := loadConfig(os.Args[1])

	if len(conf.AccessIP) > 0 {
		fmt.Printf("RTC reachable at %s\n", conf.AccessIP)
	}

	if conf.PortStart > 0 && conf.PortEnd > conf.PortStart {
		fmt.Printf("Will listen only on these ports: %d to %d\n", conf.PortStart, conf.PortEnd)
	}

	/// Avoid to quick restart loop
	defer timeBeforeQuit(&conf)

	// Then connect to websocket
	openWs(&conf)
}

func timeBeforeQuit(c *Config) {
	log.Printf("Waiting %d seconds before quitting...", c.Restart)
	time.Sleep(time.Second * time.Duration(c.Restart))
}
