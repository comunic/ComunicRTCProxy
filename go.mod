module comunic_rtc_proxy

go 1.14

require (
	github.com/gorilla/websocket v1.5.0
	github.com/pion/rtcp v1.2.10
	github.com/pion/webrtc/v2 v2.2.26
	gopkg.in/yaml.v2 v2.4.0
)
